const express = require("express");
const bodyParser = require("body-parser");
const mongoClient = require("mongodb").MongoClient;
  
const app = express();
const jsonParser = bodyParser.json();
const url = "mongodb://localhost:27017/notesdb";
  
app.use(express.static(__dirname + "/public"));
app.get("/api/notes", function(req, res){
       
    mongoClient.connect(url, function(err, client){
        client.db("notesdb").collection("notes").find({}).toArray(function(err, notes){
            res.send(notes)
            client.close();
        });
    });
});

app.post("/api/notes", jsonParser, function (req, res) {
      
    if(!req.body) return res.sendStatus(400);
      
    var noteTitle = req.body.title;
    var noteText = req.body.text;
    var note = {title: noteTitle, text: noteText};
      
    mongoClient.connect(url, function(err, client){
        client.db("notesdb").collection("notes").insertOne(note, function(err, result){           
            if(err) return res.status(400).send();     
            res.send(note);
            client.close();
        });
    });
});
    
app.listen(3000, function(){
    console.log("Сервер ожидает подключения...");
});